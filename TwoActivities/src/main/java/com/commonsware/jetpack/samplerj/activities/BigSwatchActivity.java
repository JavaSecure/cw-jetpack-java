/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;

public class BigSwatchActivity extends AppCompatActivity {
  static final String EXTRA_COLOR = "color";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_big_swatch);

    int color = getIntent().getIntExtra(EXTRA_COLOR, 0x7FFF0000);
    Toolbar toolbar = findViewById(R.id.toolbar);

    toolbar.setTitle("#" + Integer.toHexString(color));
    findViewById(R.id.swatch).setBackgroundColor(color);
  }
}
