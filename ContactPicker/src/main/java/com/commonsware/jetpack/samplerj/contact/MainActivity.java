/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.contact;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.Button;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

public class MainActivity extends AppCompatActivity {
  private static final int REQUEST_PICK = 1337;
  private ContactViewModel vm;
  private Button viewButton;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    viewButton = findViewById(R.id.view);

    vm = ViewModelProviders
      .of(this, new ContactViewModelFactory(savedInstanceState))
      .get(ContactViewModel.class);
    updateViewButton();

    findViewById(R.id.pick).setOnClickListener(v -> {
        try {
          startActivityForResult(new Intent(Intent.ACTION_PICK,
            ContactsContract.Contacts.CONTENT_URI), REQUEST_PICK);
        }
        catch (Exception e) {
          Toast.makeText(this, R.string.msg_pick_error,
            Toast.LENGTH_LONG).show();
        }
      }
    );

    viewButton.setOnClickListener(
      v -> {
        try {
          startActivity(new Intent(Intent.ACTION_VIEW, vm.contact));
        }
        catch (Exception e) {
          Toast.makeText(this, R.string.msg_view_error,
            Toast.LENGTH_LONG).show();
        }
      });
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode,
                                  @Nullable Intent data) {
    if (requestCode == REQUEST_PICK && resultCode == RESULT_OK &&
      data != null) {
      vm.contact = data.getData();
      updateViewButton();
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    vm.onSaveInstanceState(outState);
  }

  private void updateViewButton() {
    if (vm.contact != null) {
      viewButton.setEnabled(true);
    }
  }
}
