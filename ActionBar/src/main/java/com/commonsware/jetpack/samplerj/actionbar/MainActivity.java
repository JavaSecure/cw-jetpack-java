/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.actionbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
  private ColorViewModel vm;
  private ColorAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    RecyclerView items = findViewById(R.id.items);

    adapter = new ColorAdapter(getLayoutInflater());
    vm = ViewModelProviders
      .of(this, new ColorViewModelFactory(savedInstanceState))
      .get(ColorViewModel.class);
    adapter.submitList(vm.numbers);
    items.setLayoutManager(new LinearLayoutManager(this));
    items.addItemDecoration(
      new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
    items.setAdapter(adapter);

    setSupportActionBar(findViewById(R.id.toolbar));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.actions, menu);

    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.refresh) {
      vm.refresh();
      adapter.submitList(vm.numbers);
      return true;
    }
    else if (item.getItemId() == R.id.about) {
      Toast.makeText(MainActivity.this, R.string.msg_toast,
        Toast.LENGTH_LONG).show();
      return true;
    }
    else {
      return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    vm.onSaveInstanceState(outState);
  }
}
