/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.toolbar;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.Random;
import androidx.lifecycle.ViewModel;

public class ColorViewModel extends ViewModel {
  private static final String STATE_NUMBERS = "numbers";
  ArrayList<Integer> numbers;

  ColorViewModel(Bundle state) {
    if (state == null) {
      numbers = buildItems();
    }
    else {
      numbers = state.getIntegerArrayList(STATE_NUMBERS);
    }
  }

  void onSaveInstanceState(Bundle state) {
    state.putIntegerArrayList(STATE_NUMBERS, numbers);
  }

  void refresh() {
    numbers = buildItems();
  }

  private ArrayList<Integer> buildItems() {
    Random random = new Random();

    ArrayList<Integer> result = new ArrayList<>(25);

    for (int i = 0; i < 25; i++) {
      result.add(random.nextInt());
    }

    return result;
  }
}
